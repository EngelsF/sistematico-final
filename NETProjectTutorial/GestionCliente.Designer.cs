﻿namespace NETProjectTutorial
{
    partial class GestionCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnEliminarCL = new System.Windows.Forms.Button();
            this.btneditarCL = new System.Windows.Forms.Button();
            this.btnNuevoCL = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(471, 20);
            this.textBox1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 62);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(470, 328);
            this.dataGridView1.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnEliminarCL);
            this.flowLayoutPanel1.Controls.Add(this.btneditarCL);
            this.flowLayoutPanel1.Controls.Add(this.btnNuevoCL);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 396);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(470, 43);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btnEliminarCL
            // 
            this.btnEliminarCL.Location = new System.Drawing.Point(397, 3);
            this.btnEliminarCL.Name = "btnEliminarCL";
            this.btnEliminarCL.Size = new System.Drawing.Size(70, 40);
            this.btnEliminarCL.TabIndex = 0;
            this.btnEliminarCL.Text = "Eliminar";
            this.btnEliminarCL.UseVisualStyleBackColor = true;
            // 
            // btneditarCL
            // 
            this.btneditarCL.Location = new System.Drawing.Point(316, 3);
            this.btneditarCL.Name = "btneditarCL";
            this.btneditarCL.Size = new System.Drawing.Size(75, 40);
            this.btneditarCL.TabIndex = 1;
            this.btneditarCL.Text = "Editar";
            this.btneditarCL.UseVisualStyleBackColor = true;
            // 
            // btnNuevoCL
            // 
            this.btnNuevoCL.Location = new System.Drawing.Point(235, 3);
            this.btnNuevoCL.Name = "btnNuevoCL";
            this.btnNuevoCL.Size = new System.Drawing.Size(75, 40);
            this.btnNuevoCL.TabIndex = 2;
            this.btnNuevoCL.Text = "Nuevo";
            this.btnNuevoCL.UseVisualStyleBackColor = true;
            // 
            // GestionCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBox1);
            this.Name = "GestionCliente";
            this.Text = "GestionCliente";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnEliminarCL;
        private System.Windows.Forms.Button btneditarCL;
        private System.Windows.Forms.Button btnNuevoCL;
    }
}